#creating ec2 in public subnet
resource "aws_instance" "public_instance" {
  ami                         = "ami-0ed9277fb7eb570c9"
  instance_type               = "t2.micro"
  subnet_id                   = "${aws_subnet.public-subnet-1.id}"
  key_name                    = "newkey"
  count                       =  1
  associate_public_ip_address = true
  tags = {
    name = "public_instance"
}

provisioner  "remote-exec" {
  on_failure = continue 
  inline = [
    "sudo  yum -y  install httpd",
    "sudo systemctl start httpd"
  ]
  connection {
    type = "ssh"
    user = "ec2-user"
    private_key = file("./newkey.pem")
    host = self.public_ip
  }
}
}

#creating ec2 in private subnet

resource "aws_instance" "private_instance" {
  ami                         = "ami-0ed9277fb7eb570c9"
  instance_type               = "t2.micro"
  vpc_security_group_ids       =  [ "${aws_security_group.security-group.id}" ]
  subnet_id                   = "${aws_subnet.private-subnet-1.id}"
  key_name                    = "newkey"
  count                       =  1
  associate_public_ip_address =  false
  tags = {
    name = "private_instance"
  }
}
